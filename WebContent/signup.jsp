<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel = "stylesheet" href="./style.css">
<title>ユーザー登録</title>
</head>
<body>
	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<h1>登録画面</h1>
		<form action="signup" method="post">
			<br />
			<ul>
			<li class ="id">
			<label for="login_id">ログインID</label> <input name="login_id" id="login_id" /><br />
			</li>
			<li class ="name">
			<label for="name">名前</label> <input name="name" id="name" /><br />
			</li>
			<li class ="pass">
			<label for="password">パスワード</label> <input name="password" type="password" id="password" /> <br />
			</li>
			<li class ="passcon">
			<label for="passwordconfirm">確認用パスワード</label> <input name="passwordconfirm" type="password" id="passwordconfirm" /> <br />
			</li>
			<li class ="branch">
			<label for="branch_id">支店コード</label> <input name="branch_id" id="branch_id" /> <br />
			</li>
			<li class ="posision">
			<label for="position_id">部署･役職</label> <input name="position_id" id="position_id" /> <br />
		    </li>
		    <li><br /> <input type="submit" value="登録" /> <br /> <a href="management">戻る</a>
		    </li>
		    </ul>
		</form>
		<div class="copyright">Copyright(c)Your Name</div>
	</div>
</body>
</html>