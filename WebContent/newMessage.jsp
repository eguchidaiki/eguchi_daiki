<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>掲示板システム</title>
</head>
<body>
	<div class="main-contents">
		<div class="header">
			<c:if test="${ empty loginUser }">
				<a href="login">ログイン</a>
				<a href="signup">登録する</a>
			</c:if>
			<c:if test="${ not empty loginUser }">
				<a href="top">ホーム</a>
			</c:if>
		</div>

		<div class="form-area">
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>
			<c:if test="${ isShowMessageForm }">
				<form action="newMessage" method="post">
					件名<br /> <input type="text" name="title"> <br /> 投稿内容<br />
					<textarea name="message" cols="100" rows="10" class="message-box"></textarea>
					(1000文字まで) <br /> カテゴリー<br /> <input type="text" name="category">
					<br /> <input type="submit" value="投稿">

				</form>
			</c:if>
		</div>

		<div class="copyright">Copyright(c)eguchi_daiki</div>
	</div>
</body>
</html>