<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー管理</title>
<script type="text/javascript">


function stop(){

	// 「OK」時の処理開始 ＋ 確認ダイアログの表示
	if(window.confirm('停止しますか？')){

		return true;

	}
	// 「OK」時の処理終了

	// 「キャンセル」時の処理開始
	else{

		return false;

	}
	// 「キャンセル」時の処理終了

}
function recovery(){

	// 「OK」時の処理開始 ＋ 確認ダイアログの表示
	if(window.confirm('復活しますか？')){

		return true;

	}
	// 「OK」時の処理終了

	// 「キャンセル」時の処理開始
	else{

		return false;

	}
	// 「キャンセル」時の処理終了

}


</script>

</head>
<body>
	<div class="main-contents">
		<div class="header">
			<c:if test="${ not empty loginUser }">
				<a href="top">ホーム</a>
				<a href="signup">新規登録</a>
			</c:if>
		</div>
		
    <div class="profile">
    
    <table border="1">
    <tr>
    <th>名前</th>
    <th>ログインID</th>
    <th>支店コード</th>
    <th>部署･役職コード</th>
    <th>編集</th>
    </tr>
    <c:forEach items="${management}" var="management">
    <tr>
    <td><c:out value="${management.name}" /></td>
    <td><c:out value="${management.login_id}" /></td>
    <td><c:out value="${management.branchName}" /></td>
    <td><c:out value="${management.positionName}" /></td>
    <td><a href= "settings?id=${management.id}">編集</a></td>
    <c:if test="${loginUser.id != management.id }">
    <c:if test = "${management.stop_id == 0 }">
    <td><form action="stop" method="post" onSubmit="return stop()">
			<input type="hidden" name = "stop_id" value = "1">
			<input type="hidden" name = "id" value = "${management.id}">
			<input type="submit" value="停止" >
			
		</form>
	</td>
	</c:if>
	
	<c:if test = "${management.stop_id == 1 }">
	<td><form action="stop" method="post"onClick="return recovery()">
			<input type="hidden" name = "stop_id" value = "0">
			<input type="hidden" name = "id" value = "${management.id}">
			<input type="submit" value="復活">
			
		</form>
	</td>
	</c:if>
	</c:if>
    </tr>
        </c:forEach>
    </table>
        </div>
    
</div>

		<div class="copyright">Copyright(c)eguchi_daiki</div>
	
</body>
</html>