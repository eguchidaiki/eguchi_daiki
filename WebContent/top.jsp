<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <link href="./css/style.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>掲示板システム</title>
</head>
<body>
	<div class="main-contents">

		<div class="header">
			<c:if test="${ empty loginUser }">
				<a href="login">ログイン</a>
			</c:if>
			<c:if test="${ not empty loginUser }">
			<c:out value="ログイン中:${loginUser.name}" />
				<a href="management">ユーザー管理</a>
				<a href="newMessage">新規投稿</a>
				<a href="logout">ログアウト</a>
			</c:if>
		</div>
		
        <div class="errorMessages">
        
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
			
		<div class="messages">
		<form name = "top" method = "get">
		<table border="1">
		<tr>
		<td>カテゴリー<input type="text" name = "serch" value="${serch}" ></td>
		</tr>
		<tr>
		<td>投稿日<input type="date" name=date value="${date}" >ー<input type ="date" name="last_date"value="${last_date}" ></td>
		<td><input type="submit"value = "絞り込み"></td>
		</tr>
		</table>
		</form>
		
			<c:forEach items="${messages}" var="message">
			<c:if test = "${message.is_delete == 0}">
				<div class="message">
					<div class="name">
						<c:out value="投稿者:${message.name}" />
					</div>
					<div class="title">
						<c:out value="件名:${message.title}" />
					</div>
					<div class="text">
						<c:out value="本文:${message.text}" />
					</div>
					<div class="category">
						<c:out value="カテゴリー:${message.category}" />
					</div>
					<div class="date">
						<fmt:formatDate value="${message.created_date}"
							pattern="yyyy/MM/dd HH:mm:ss" />
					</div>
					<c:if test="${loginUser.id == message.userId }">
					<form action="is_delete" method="post">
							<input type = "hidden" name = "is_delete" value = "1">
							<input type = "hidden" name = "id" value = "${message.id}">
							     <input type="submit" value="削除">
							</form>
							</c:if>
<hr/>
<hr/>
				</div>
				
				<c:if test="${ not empty errorComments }">
				<c:if test="${message.id == errorCommentsId}">
                <div class="errorComments">
                    <ul>
                        <c:forEach items="${errorComments}" var="comment">
                            <li><c:out value="${comment}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorComments" scope="session"/>
                </c:if>
            </c:if>

				<form action="comment" method="post">
					コメント<br />
					<textarea name="comment" cols="100" rows="5" class="comment-box"></textarea>
					<br /> <input type="hidden" name="message_id" value="${message.id}"> 
						<input type="submit" value="送信">（500文字まで）
				</form>
				
				<hr>
				
				<c:forEach items="${comments}" var="comment">
				<c:if test = "${comment.comment_delete == 0}">
					<c:if test="${message.id == comment.commentId}">
						<div class="comment">
							<span class="name">
								<c:out value="${comment.name}" />
							</span>
							<span class="text">
								<c:out value="コメント:${comment.text}" />
							</span>
							<span class="date">
								<fmt:formatDate value="${comment.created_date}"
									pattern="yyyy/MM/dd HH:mm:ss" />
							</span>
					
						 <c:if test="${loginUser.id == comment.userId }">
							<form action="comment_delete" method="post">
							<input type = "hidden" name = "comment_delete" value = "1">
							<input type = "hidden" name = "id" value = "${comment.id}">
							     <input type="submit" value="削除">
							</form>
							</c:if>
						
							<hr>
						</div>

					</c:if>
					</c:if>
				</c:forEach>
				<hr>
				
</c:if>
			</c:forEach>

		</div>
		<div class="copyright">Copyright(c)eguchi_daiki</div>
	</div>
</body>
</html>