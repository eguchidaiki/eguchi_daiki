package eguchi_daiki.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import eguchi_daiki.beans.User;

/**
 * Servlet Filter implementation class authorityfilter
 */
@WebFilter(urlPatterns = {"/*"})
public class loginfilter implements Filter {
public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
    String target = ((HttpServletRequest)request).getServletPath();
	HttpSession session = ((HttpServletRequest)request).getSession();
	 User loginUser = (User) session.getAttribute("loginUser");
	
	if(target.equals("/login")) {
		chain.doFilter(request,response);
	}
	else if (loginUser != null) {
	if(target.equals("/management")||target.equals("/settings")||target.equals("/signup")) {
	if (loginUser.getPosition_id() != 1 ) {
		String messages = "権限がありません";
		session.setAttribute("errorMessages", messages);
		((HttpServletResponse)response).sendRedirect("top");
		return;
	}
	}
	chain.doFilter(request,response);
	}else {
		List<String> messages = new ArrayList<String>();
        messages.add("ログインしてください。");
        session.setAttribute("errorMessages", messages);
		((HttpServletResponse)response).sendRedirect("login");
	}
	
}

@Override
public void destroy() {
	// TODO 自動生成されたメソッド・スタブ
	
}

@Override
public void init(FilterConfig filterConfig) throws ServletException {
	// TODO 自動生成されたメソッド・スタブ
	
}
}