package eguchi_daiki.service;

import static eguchi_daiki.utils.CloseableUtil.*;
import static eguchi_daiki.utils.DBUtil.*;

import java.sql.Connection;

import eguchi_daiki.beans.UserMessage;
import eguchi_daiki.dao.Is_deleteDao;

public class Is_deleteService {
	 public void delete(UserMessage is_delete) {

         Connection connection = null;
         try {
             connection = getConnection();

             Is_deleteDao is_deleteDao = new Is_deleteDao();
             is_deleteDao.update(connection, is_delete);

             commit(connection);
         } catch (RuntimeException e) {
             rollback(connection);
             throw e;
         } catch (Error e) {
             rollback(connection);
             throw e;
         } finally {
             close(connection);
         }
     }
	 
}
