package eguchi_daiki.service;

import static eguchi_daiki.utils.CloseableUtil.*;
import static eguchi_daiki.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import eguchi_daiki.beans.Management;
import eguchi_daiki.dao.ManagementDao;

public class ManagementService {

    public void register(Management management) {

        Connection connection = null;
        try {
            connection = getConnection();


            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    private static final int LIMIT_NUM =1000;


    public List<Management> getManagement() {

        Connection connection = null;
        try {
            connection = getConnection();

            ManagementDao ManagementDao = new ManagementDao();
            List<Management> ret = ManagementDao.getManagement(connection, LIMIT_NUM);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}

