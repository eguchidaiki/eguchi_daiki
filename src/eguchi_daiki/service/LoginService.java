package eguchi_daiki.service;

import static eguchi_daiki.utils.CloseableUtil.*;
import static eguchi_daiki.utils.DBUtil.*;

import java.sql.Connection;

import eguchi_daiki.beans.User;
import eguchi_daiki.dao.UserDao;
import eguchi_daiki.utils.CipherUtil;

public class LoginService {

    public User login(String login_id, String password) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            String encPassword = CipherUtil.encrypt(password);
            User user = userDao.getUser(connection, login_id, encPassword);

            commit(connection);

            return user;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}
