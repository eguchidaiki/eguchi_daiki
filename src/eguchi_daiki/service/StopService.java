package eguchi_daiki.service;

import static eguchi_daiki.utils.CloseableUtil.*;
import static eguchi_daiki.utils.DBUtil.*;

import java.sql.Connection;

import eguchi_daiki.beans.Management;
import eguchi_daiki.dao.StopDao;

public class StopService {
	 public void stop(Management stop_id) {

         Connection connection = null;
         try {
             connection = getConnection();

             StopDao stopDao = new StopDao();
             stopDao.update(connection, stop_id);

             commit(connection);
         } catch (RuntimeException e) {
             rollback(connection);
             throw e;
         } catch (Error e) {
             rollback(connection);
             throw e;
         } finally {
             close(connection);
         }
     }
	 
}

