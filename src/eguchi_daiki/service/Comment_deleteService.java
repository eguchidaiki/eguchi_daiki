package eguchi_daiki.service;

import static eguchi_daiki.utils.CloseableUtil.*;
import static eguchi_daiki.utils.DBUtil.*;

import java.sql.Connection;

import eguchi_daiki.beans.UserComment;
import eguchi_daiki.dao.Comment_deleteDao;

public class Comment_deleteService {
	 
	 public void comment_delete(UserComment comment_delete) {

         Connection connection = null;
         try {
             connection = getConnection();

             Comment_deleteDao comment_deleteDao = new Comment_deleteDao();
             comment_deleteDao.comment_update(connection, comment_delete);

             commit(connection);
         } catch (RuntimeException e) {
             rollback(connection);
             throw e;
         } catch (Error e) {
             rollback(connection);
             throw e;
         } finally {
             close(connection);
         }
     }

}

