package eguchi_daiki.service;

import static eguchi_daiki.utils.CloseableUtil.*;
import static eguchi_daiki.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import eguchi_daiki.beans.Message;
import eguchi_daiki.beans.UserMessage;
import eguchi_daiki.dao.MessageDao;
import eguchi_daiki.dao.UserMessageDao;

public class MessageService {

    public void register(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();

            MessageDao messageDao = new MessageDao();
            messageDao.insert(connection, message);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    private static final int LIMIT_NUM = 1000;

    public List<UserMessage> getMessage(String serch,String date, String last_date) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserMessageDao messageDao = new UserMessageDao();
            List<UserMessage> ret = messageDao.getUserMessages(connection, LIMIT_NUM, serch , date, last_date);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}
