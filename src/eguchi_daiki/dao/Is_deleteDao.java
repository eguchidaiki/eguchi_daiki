package eguchi_daiki.dao;

import static eguchi_daiki.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import eguchi_daiki.beans.UserMessage;
import eguchi_daiki.exception.SQLRuntimeException;

public class Is_deleteDao {

	public void update(Connection connection, UserMessage usermessage) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE messages SET is_delete = ? , updated_date = CURRENT_TIMESTAMP WHERE id = ? ");
            
            

            ps = connection.prepareStatement(sql.toString());
            ps.setInt(1, usermessage.getIs_delete());
            ps.setInt(2, usermessage.getId());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
	
}