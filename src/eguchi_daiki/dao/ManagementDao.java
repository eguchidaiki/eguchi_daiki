package eguchi_daiki.dao;

import static eguchi_daiki.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import eguchi_daiki.beans.Management;
import eguchi_daiki.exception.SQLRuntimeException;

public class ManagementDao {

    public List<Management> getManagement(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("users.id as id,");
            sql.append("users.login_id as login_id, ");
            sql.append("users.name as name, ");
            sql.append("branches.name as branchName, ");
            sql.append("positions.name as positionName, ");
            sql.append("users.created_date as created_date, ");
            sql.append("users.stop_id as stop_id ");
            sql.append("FROM users ");
            sql.append("INNER JOIN branches ");
            sql.append("ON users.branch_id = branches.id ");
            sql.append("INNER JOIN positions ");
            sql.append("ON users.position_id = positions.id ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<Management> ret = tomanagementList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<Management> tomanagementList(ResultSet rs)
            throws SQLException {

        List<Management> ret = new ArrayList<Management>();
        try {
            while (rs.next()) {
            	int id = rs.getInt("id");
            	String login_id = rs.getString("login_id");
                String name = rs.getString("name");
                String branch_id = rs.getString("branchName");
                String position_id = rs.getString("positionName");
                Timestamp createdDate = rs.getTimestamp("created_date");
                int stop_id = rs.getInt("stop_id");

                Management management = new Management();
                management.setId(id);
                management.setLogin_id(login_id);
                management.setName(name);
                management.setBranchName(branch_id);
                management.setPositionName(position_id);
                management.setCreatedDate(createdDate);
                management.setStop_id(stop_id);

                ret.add(management);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}
