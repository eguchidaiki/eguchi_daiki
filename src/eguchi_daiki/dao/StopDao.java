package eguchi_daiki.dao;

import static eguchi_daiki.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import eguchi_daiki.beans.Management;
import eguchi_daiki.exception.SQLRuntimeException;

public class StopDao {

	public void update(Connection connection, Management management) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET stop_id = ? , updated_date = CURRENT_TIMESTAMP WHERE id = ? ");
            
            

            ps = connection.prepareStatement(sql.toString());
            ps.setInt(1, management.getStop_id());
            ps.setInt(2, management.getId());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
	
}