package eguchi_daiki.dao;

import static eguchi_daiki.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import eguchi_daiki.beans.UserComment;
import eguchi_daiki.exception.SQLRuntimeException;

public class Comment_deleteDao {

	
	public void comment_update(Connection connection, UserComment usercomment) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE comments SET comment_delete = ? , updated_date = CURRENT_TIMESTAMP WHERE id = ? ");
            
            

            ps = connection.prepareStatement(sql.toString());
            ps.setInt(1, usercomment.getComment_delete());
            ps.setInt(2, usercomment.getId());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
}