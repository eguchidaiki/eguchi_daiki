package eguchi_daiki.dao;

import static eguchi_daiki.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import eguchi_daiki.beans.UserMessage;
import eguchi_daiki.exception.SQLRuntimeException;

public class UserMessageDao {

    public List<UserMessage> getUserMessages(Connection connection, int num, String serch ,String date ,String last_date) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("messages.id as id,");
            sql.append("messages.user_id as user_id, ");
            sql.append("messages.title as title, ");
            sql.append("messages.text as text, ");
            sql.append("messages.category as category, ");
            sql.append("users.name as name, ");
            sql.append("messages.created_date as created_date, ");
            sql.append("messages.is_delete as is_delete ");
            sql.append("FROM messages ");
            sql.append("INNER JOIN users ");
            sql.append("ON messages.user_id = users.id ");
            if(StringUtils.isEmpty(date)) {
            	date = "2019/01/01";
            }
            if(StringUtils.isEmpty(last_date)) {
            	last_date = "2019/12/30";
            }
            sql.append("where messages.created_date between"+ "'" + date + "'" + "and" + "'"+ last_date +"'" );
            
            if(!(StringUtils.isEmpty(serch))) {
            sql.append("and category like" + "'%"+  serch + "%'");
            }
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserMessage> ret = toUserMessageList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserMessage> toUserMessageList(ResultSet rs)
            throws SQLException {

        List<UserMessage> ret = new ArrayList<UserMessage>();
        try {
            while (rs.next()) {
            	int id = rs.getInt("id");
            	int user_id = rs.getInt("user_id");
                String title = rs.getString("title");
                String name = rs.getString("name");
                String category = rs.getString("category");
                String text = rs.getString("text");
                Timestamp createdDate = rs.getTimestamp("created_date");
                int is_delete = rs.getInt("is_delete");

                UserMessage message = new UserMessage();
                message.setId(id);
                message.setUserId(user_id);
                message.setTitle(title);
                message.setName(name);
                message.setCategory(category);
                message.setText(text);
                message.setCreated_date(createdDate);
                message.setIs_delete(is_delete);

                ret.add(message);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}
