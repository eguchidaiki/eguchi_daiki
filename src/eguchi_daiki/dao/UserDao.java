package eguchi_daiki.dao;

import static eguchi_daiki.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import eguchi_daiki.beans.User;
import eguchi_daiki.exception.NoRowsUpdatedRuntimeException;
import eguchi_daiki.exception.SQLRuntimeException;

public class UserDao {

    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("login_id");
            sql.append(", name");
            sql.append(", password");
            sql.append(", branch_id");
            sql.append(", position_id");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append(" ?");//login_id
            sql.append(", ?"); // name
            sql.append(", ?"); // password
            sql.append(", ?"); //branch_id
            sql.append(", ?"); //position_id
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getLogin_id());
            ps.setString(2, user.getName());
            ps.setString(3, user.getPassword());
            ps.setInt(4, user.getBranch_id());
            ps.setInt(5, user.getPosition_id());
            System.out.println(ps.toString());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    
    
    public User getUser(Connection connection, String login_id,
            String password) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE login_id = ? AND password = ?";

            ps = connection.prepareStatement(sql);
            ps.setString(1, login_id);
            ps.setString(2, password);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<User> toUserList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
            	int id = rs.getInt("id");
                String login_id = rs.getString("login_id");
                String name = rs.getString("name");
                String password = rs.getString("password");
                int branch_id =rs.getInt("branch_id");
                int position_id = rs.getInt("position_id");
                Timestamp createdDate = rs.getTimestamp("created_date");
                Timestamp updatedDate = rs.getTimestamp("updated_date");
                int stop_id = rs.getInt("stop_id");

                User user = new User();
                user.setId(id);
                user.setLogin_id(login_id);
                user.setName(name);
                user.setPassword(password);
                user.setBranch_id(branch_id);
                user.setPosition_id(position_id);
                user.setCreatedDate(createdDate);
                user.setUpdatedDate(updatedDate);
                user.setStop_id(stop_id);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
    
    public User getUser(Connection connection, int id) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE id = ?";

            ps = connection.prepareStatement(sql);
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public String check(Connection connection, String login_id) {
        PreparedStatement ps = null;
        try {
            String sql = "select count(login_id) as count from users where login_id = ?";

            ps = connection.prepareStatement(sql);
            ps.setString(1, login_id);

            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
            if (rs.getInt("count")>0) {
            	String errorMessege = "このIDは既に登録されています";
            	return errorMessege;
            }
            else {
            	return null;
            }
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
		return null;
    }
    public void update(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET");
            sql.append("  login_id = ?");
            sql.append(", name = ?");
            sql.append(", password = ?");
            sql.append(", branch_id = ?");
            sql.append(", position_id = ?");
            sql.append(", updated_date = CURRENT_TIMESTAMP");
            sql.append(" WHERE");
            sql.append("  id = ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getLogin_id());
            ps.setString(2, user.getName());
            ps.setString(3, user.getPassword());
            ps.setInt(4, user.getBranch_id());
            ps.setInt(5, user.getPosition_id());
            ps.setInt(6, user.getId());
System.out.println(ps.toString());
            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }

    }


}