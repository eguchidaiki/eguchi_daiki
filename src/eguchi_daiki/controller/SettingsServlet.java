package eguchi_daiki.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import eguchi_daiki.beans.User;
import eguchi_daiki.exception.NoRowsUpdatedRuntimeException;
import eguchi_daiki.service.UserService;

@WebServlet(urlPatterns = { "/settings" })
public class SettingsServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        
        //セッションよりログインユーザーの情報を取得
        //ログインユーザー情報のidを元にDBからユーザー情報取得
        
        int id = Integer.parseInt(request.getParameter("id"));
        User editUser = new UserService().getUser(id);
        request.setAttribute("editUser", editUser);

        request.getRequestDispatcher("settings.jsp").forward(request, response);
    }
    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        List<String> messages = new ArrayList<String>();
        HttpSession session = request.getSession();
        User editUser = getEditUser(request);

        if (isValid(request, messages) == true) {

            try {
                new UserService().update(editUser);
            } catch (NoRowsUpdatedRuntimeException e) {
                messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
                session.setAttribute("errorMessages", messages);
                request.setAttribute("editUser", editUser);
                request.getRequestDispatcher("settings.jsp").forward(request, response);
                return;
            }

            session.setAttribute("user", editUser);

            response.sendRedirect("management");
        } else {
            session.setAttribute("errorMessages", messages);
            request.setAttribute("editUser", editUser);
            request.getRequestDispatcher("settings.jsp").forward(request, response);
        }
    }
    private User getEditUser(HttpServletRequest request)
            throws IOException, ServletException {

        User editUser = new User();
        editUser.setId(Integer.parseInt(request.getParameter("id")));
        editUser.setLogin_id(request.getParameter("login_id"));
        editUser.setName(request.getParameter("name"));
        editUser.setPassword(request.getParameter("password"));
        editUser.setPassword(request.getParameter("passwordconfirm"));
        editUser.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
        editUser.setPosition_id(Integer.parseInt(request.getParameter("position_id")));
        return editUser;
    }
    
    public boolean isMainPosition(String position_id) {
    	if(position_id.equals("1") || position_id.equals("2")) {
    		return true;
    	}
		return false;
    }
    
    private boolean isValid(HttpServletRequest request, List<String> messages) {
    	
    	int id = Integer.parseInt(request.getParameter("id"));
        String login_id = request.getParameter("login_id");
        String password = request.getParameter("password");
        String passwordconfirm = request.getParameter("passwordconfirm");
        String name = request.getParameter("name");
        String check = new UserService().check(login_id);
        String position_id = request.getParameter("position_id");
        String branch_id = request.getParameter("branch_id");
        User editUser = new UserService().getUser(id);
        
        	
        if (!login_id.equals(editUser.getLogin_id())) {
        if (check != null) {
        	messages.add(check);
        }
        }
        if (StringUtils.isEmpty(login_id) == true) {
            messages.add("ログインIDを入力してください");
        }
        if (!(StringUtils.isEmpty(login_id) == true)) {
        if (!(login_id.matches("^[a-zA-Z0-9]+$"))) {
        	messages.add("ログインIDは半角英数字で入力してください。");
        }
        }
        if(StringUtils.isEmpty(name)) {
        	messages.add("名前を入力してください");
        }
        if (StringUtils.isEmpty(password) == true) {
            messages.add("パスワードを入力してください");
        }
        if(!(password.equals(passwordconfirm))) {
        	messages.add("確認用パスワードと一致しません");
        }
        if(!StringUtils.isEmpty(password)) {
        if (!((password.length() >= 6) && (password.length() <= 20 ))) {
            messages.add("パスワードが6文字以上20文字以下ではありません。");
        }
        }
        if (!StringUtils.isEmpty(login_id)) {
        if (!((login_id.length() >= 6) && (login_id.length() <= 20 ))) {
                messages.add("ログインIDが6文字以上20文字以下ではありません");
        }    
        }
        if(name.length() > 10) {
        	messages.add("名前は10文字以下で入力してください。");
        }
        if(password.matches( "-~｡-ﾟ")) {
        	messages.add("パスワードは半角文字で入力してください。");
        }

        if(branch_id.equals("1") && !isMainPosition(position_id)) {
        	messages.add("支店と部署・役職の組み合わせが不正です");
        }
        if(!(branch_id.equals("1")) && isMainPosition(position_id)) {
        	messages.add("支店と部署・役職の組み合わせが不正です");
        }
    
        // TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}
