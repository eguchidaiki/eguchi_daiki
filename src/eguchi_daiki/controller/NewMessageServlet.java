package eguchi_daiki.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import eguchi_daiki.beans.Message;
import eguchi_daiki.beans.User;
import eguchi_daiki.beans.UserMessage;
import eguchi_daiki.service.MessageService;

@WebServlet(urlPatterns = { "/newMessage" })
public class NewMessageServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        User user = (User) request.getSession().getAttribute("loginUser");
        boolean isShowMessageForm;
        if (user != null) {
            isShowMessageForm = true;
        } else {
            isShowMessageForm = false;
        }

        List<UserMessage> messages = new MessageService().getMessage(request.getParameter("serch"),request.getParameter("date"),request.getParameter("last_date"));

        request.setAttribute("messages", messages);
        request.setAttribute("isShowMessageForm", isShowMessageForm);


        request.getRequestDispatcher("/newMessage.jsp").forward(request, response);
    }


    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();

        List<String> messages = new ArrayList<String>();

        if (isValid(request, messages) == true) {

        	User user = (User) session.getAttribute("loginUser");

            Message message = new Message();
            message.setText(request.getParameter("message"));
            message.setTitle(request.getParameter("title"));
            message.setCategory(request.getParameter("category"));
            message.setUser_id(user.getId());

            new MessageService().register(message);

            response.sendRedirect("top");
        } else {
            session.setAttribute("errorMessages", messages);
            response.sendRedirect("newMessage");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {

        String message = request.getParameter("message");
        String title = request.getParameter("title");
        String category = request.getParameter("category");

        if (StringUtils.isEmpty(message) == true) {
            messages.add("メッセージを入力してください");
        }
        if (StringUtils.isEmpty(title) == true) {
            messages.add("件名を入力してください");
        }
        if(title.length() > 30) {
        	messages.add("件名は30文字以下で入力してください。");
        }
        if (StringUtils.isEmpty(category) == true) {
            messages.add("カテゴリーを入力してください");
        }
        if (category.length()>10) {
        	messages.add("カテゴリーは10字以内で入力してください。");
        }
        if (1000 < message.length()) {
            messages.add("1000文字以下で入力してください");
        }
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}
