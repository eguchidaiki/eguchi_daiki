package eguchi_daiki.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import eguchi_daiki.beans.User;
import eguchi_daiki.service.LoginService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("login.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
    	
    	HttpSession session = request.getSession();
        List<String> messages = new ArrayList<String>();
        LoginService loginService = new LoginService();
        String login_id = request.getParameter("login_id");
        String password = request.getParameter("password");
        
        if (login_id.isEmpty() || password.isEmpty()) {
        	messages.add("ログインIDまたはパスワードが入力されていません");
            session.setAttribute("errorMessages", messages);
            request.getRequestDispatcher("login.jsp").forward(request, response);
            return;
        }
        User user = loginService.login(login_id, password);

        
        if (user != null && user.getStop_id() == 0) {
        	session.setAttribute("loginUser", user);

        } else {
            
            messages.add("ログインIDまたはパスワードが間違っています");
            session.setAttribute("errorMessages", messages);
            session.setAttribute("loginUser", user);
            request.getRequestDispatcher("login.jsp").forward(request, response);
        }
        response.sendRedirect("top");
        


            
    }
    

}
