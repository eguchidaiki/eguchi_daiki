package eguchi_daiki.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import eguchi_daiki.beans.Management;
import eguchi_daiki.service.StopService;


@WebServlet(urlPatterns = { "/stop" })
public class StopServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    
    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
    	
    	Management stop_id = new Management();
    	stop_id.setStop_id(Integer.parseInt(request.getParameter("stop_id")));
    	stop_id.setId(Integer.parseInt(request.getParameter("id")));
    	new StopService().stop(stop_id);
    	response.sendRedirect("management");
    	
    }
    
}
