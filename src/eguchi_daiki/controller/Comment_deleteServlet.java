package eguchi_daiki.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import eguchi_daiki.beans.UserComment;
import eguchi_daiki.service.Comment_deleteService;


@WebServlet(urlPatterns = { "/comment_delete" })
public class Comment_deleteServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {	
    	UserComment comment_delete = new UserComment();
    	comment_delete.setComment_delete(Integer.parseInt(request.getParameter("comment_delete")));
    	comment_delete.setId(Integer.parseInt(request.getParameter("id")));
    	new Comment_deleteService().comment_delete(comment_delete);
    	response.sendRedirect("top");
    	
    }
    
}
