package eguchi_daiki.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import eguchi_daiki.beans.Management;
import eguchi_daiki.service.ManagementService;


@WebServlet(urlPatterns = { "/management" })
public class ManagementServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
    	List<Management> management = new ManagementService().getManagement();
    	request.setAttribute("management",management );
    	
        request.getRequestDispatcher("management.jsp").forward(request, response);
    }
}
