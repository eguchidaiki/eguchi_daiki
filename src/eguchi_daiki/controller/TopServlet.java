package eguchi_daiki.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import eguchi_daiki.beans.User;
import eguchi_daiki.beans.UserComment;
import eguchi_daiki.beans.UserMessage;
import eguchi_daiki.service.CommentService;
import eguchi_daiki.service.MessageService;

@WebServlet(urlPatterns = { "/top" })
public class TopServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        User user = (User) request.getSession().getAttribute("loginUser"); 
        boolean isShowMessageForm;
        if (user != null) {
            isShowMessageForm = true;
        } else {
            isShowMessageForm = false;
        }
        

        List<UserMessage> messages = new MessageService().getMessage(request.getParameter("serch"),request.getParameter("date"),request.getParameter("last_date"));
        String serch =request.getParameter("serch");	
        String date = request.getParameter("date");
        String last_date = request.getParameter("last_date");
        request.setAttribute("serch", serch);
        request.setAttribute("date", date);
        request.setAttribute("last_date", last_date);

        request.setAttribute("messages", messages);
        request.setAttribute("isShowMessageForm", isShowMessageForm);
        
        
        List<UserComment> comments = new CommentService().getComment();
        request.setAttribute("comments", comments);

        request.getRequestDispatcher("top.jsp").forward(request, response);
        
        
    }

}