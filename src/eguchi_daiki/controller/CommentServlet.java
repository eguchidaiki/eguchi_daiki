package eguchi_daiki.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import eguchi_daiki.beans.Comment;
import eguchi_daiki.beans.User;
import eguchi_daiki.service.CommentService;

@WebServlet(urlPatterns = { "/comment" })
public class CommentServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();

        List<String> comments = new ArrayList<String>();
        String messageId = request.getParameter("message_id");

        if (isValid(request, comments) == true) {
        	User user = (User) session.getAttribute("loginUser");


            Comment comment = new Comment();
            comment.setText(request.getParameter("comment"));
            comment.setUserId(user.getId());
            comment.setCommentId(Integer.parseInt(messageId));

            new CommentService().register(comment);

            response.sendRedirect("top");
        } else {
            session.setAttribute("errorComments", comments);
            session.setAttribute("errorCommentsId", messageId);
            response.sendRedirect("top");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> comments) {

        String comment = request.getParameter("comment");

        if (StringUtils.isEmpty(comment) == true) {
            comments.add("コメントを入力してください");
        }
        if(comment.length()>500) {
        	comments.add("コメントは500文字以下で入力してください。");
        	
        }
        if (comments.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}