package eguchi_daiki.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import eguchi_daiki.beans.UserMessage;
import eguchi_daiki.service.Is_deleteService;


@WebServlet(urlPatterns = { "/is_delete" })
public class Is_deleteServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
    	UserMessage is_delete = new UserMessage();
    	is_delete.setIs_delete(Integer.parseInt(request.getParameter("is_delete")));
    	is_delete.setId(Integer.parseInt(request.getParameter("id")));
    	new Is_deleteService().delete(is_delete);
    	response.sendRedirect("top");
    	
    }
    
}